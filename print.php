<?php

require_once __DIR__ . '/assets/vendor/autoload.php';

require 'functions.php';
$jadwal = query("SELECT * FROM jadwal ORDER BY id_jadwal");

$mpdf = new \Mpdf\Mpdf();

$html = '<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Daftar Keseharian</title>
                <link rel="stylesheet" href="style.css">
            </head>
            <body>
            <h2>Daftar keseharian saat ini</h2>
            <hr>
            <table border="1" cellpadding="10" cellspacing="0" id="t01">
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Gambar</th>
                <th>Waktu</th>
                <th>Hari</th>
                <th>Manfaat</th>
            </tr>';

            $i = 1;
        foreach ($jadwal as $j) {
            $html .= '<tr>
                    <td>'. $i++ .'</td>
                    <td>'. $j["judul"] .'</td>
                    <td><img src="assets/img_tab/'. $j['gambar'] . '" width="80" height="50"></td>
                    <td>'. substr($j["waktu"],0,-3) .'</td>
                    <td>'. $j["hari"] .'</td>
                    <td>'. $j["ket"] .'</td>
            </tr>';
        }

$html .= '</table>
                
            </body>
        </html>';

$mpdf->WriteHTML($html);
$mpdf->Output();

?>