<?php 
session_start();

if( !isset($_SESSION["login"]) ) {
    header("Location: logindanregis.php");
    exit;
}

require_once 'functions.php';
//cek apakah tombol submit sudah ditekan atau belom
if ( isset($_POST["submit"]) ) {
    
    // cek apakah data berhasil ditambahkan atau tidak
    if( tambah($_POST) > 0 ) {
        echo "
            <script>
                alert('data berhasil ditambahkan!');
                document.location.href = 'index.php';
            </script>
        
        ";
    } else {
        echo "
            <script>
                alert('data gagal ditambahkan!');
                document.location.href = 'index.php';
            </script>
        ";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Keseharian</title>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <link rel="stylesheet" href="style.css">
    <style>
        th, td {
            padding: 0px;
            text-align: center;
        }
        input, select, textarea{
            padding: 15px;
            width: 100%;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="sidebar">
    <h2>Fauzi</h2>
    <ul>
    <li><a href="index.php"><i class="fas fa-home"></i>Home</a></li>
    <li><a href="input.php"><i class="fas fa-plus"></i>Tambah data</a></li>
    </ul>
    <div class="social_media">
        <a href="https://www.facebook.com/profile.php?id=100009076621845" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="https://twitter.com/Fauzi33372888" target="_blank"><i class="fab fa-twitter"></i></a>
        <a href="https://www.instagram.com/jipay_fp/" target="_blank"><i class="fab fa-instagram"></i></a>
    </div>
    </div>
    <div class="main_content">
        <div class="header">Welcome!! </div>


        <form action="" method="post" enctype="multipart/form-data">
        <table id='t01'>
    <caption><h2>Input Data</h2></caption>
        <tr>
            <th>Judul</th>
            <td>:</td>
            <td class="add"><input type="text" name="judul" required></td>
        </tr>
        <tr>
            <th>Gambar</th>
            <td>:</td>
            <td><input type="file" name="gambar"></td>
        </tr>
        <tr>
            <th>Hari</th>
            <td>:</td>
            <td>
            <select name="hari" id="">
                <option value="senin">senin</option>
                <option value="selasa">selasa</option>
                <option value="rabu">rabu</option>
                <option value="kamis">kamis</option>
                <option value="jumat">jumat</option>
                <option value="sabtu">sabtu</option>
                <option value="minggu">minggu</option>
            </select>
            </td>
        </tr>
        <tr>
            <th>Waktu</th>
            <td>:</td>
            <td><input type="time" name="waktu"></td>
        </tr>
        <tr>
            <th>Keterangan</th>
            <td>:</td>
            <td>
            <textarea name="ket" id="" cols="30" rows="10">
            
            </textarea>
            </td>
        </tr>
        <tr>
            <th>Action</th>
            <td>:</td>
            <td><input type="submit" value="Insert" style=background:tomato name="submit"></td>
        </tr>
    </table>
    </form>
    </div>
</div>


    
</body>
</html>