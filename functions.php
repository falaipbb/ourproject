<?php
// koneksi ke database
$conn = mysqli_connect("localhost","root","","pay");

function query($query) {
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while( $row = mysqli_fetch_assoc($result) ) {
        $rows[] = $row;
    }
    return $rows;
}


function tambah($data){
    global $conn;

    $judul = htmlspecialchars($data["judul"]);
    $hari = htmlspecialchars($data["hari"]);
    $waktu = htmlspecialchars($data["waktu"]);
    $ket = htmlspecialchars($data["ket"]);

    // upload gambar
    $gambar = upload();
    if ( !$gambar ) {
        return false;
    }

    //query insert data
    $query = "INSERT INTO jadwal
                VALUES
            ('', '$judul', '$gambar', '$hari', '$waktu', '$ket')
            
        ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function upload() {
    
    $namaFile = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpName = $_FILES['gambar']['tmp_name'];

    // cek apakah tidak ada gambar yang di upload
    // pesan error dalam upload number 4
    if( $error === 4 ) {
        echo "<script>
                alert('pilih gambar terlebih dahulu!');
            </script>
        ";
        return false;

    }


    // cek apakah yang diupload adalah gambar
    $ekstensiGambarValid = ['jpg','jpeg','png'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if ( !in_array($ekstensiGambar, $ekstensiGambarValid) ) {
        echo "<script>
                alert('yang anda upload bukan gambar!');
            </script>
            ";
            return false;
    }


    // cek jika ukurannya terlalu besar
    if ( $ukuranFile > 1000000 ) {
        echo "<script>
                alert('ukuran gambar terlalu besar!');
            </script>";
        return false;
    } 
    
    // lolos pengecekan, gambar siap diupload
    //generate nama gambar baru
    $namaFileBaru  = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    move_uploaded_file($tmpName, 'assets/img_tab/' . $namaFileBaru);

    return $namaFileBaru;
}

function hapus($id) {
    global $conn;
    mysqli_query($conn, "DELETE FROM jadwal WHERE id_jadwal = $id");
    return mysqli_affected_rows($conn);
}

function ubah($data) {
    global $conn;

    $id = $data["id"];
    $judul = htmlspecialchars($data["judul"]);
    $hari = htmlspecialchars($data["hari"]);
    $waktu = htmlspecialchars($data["waktu"]);
    $ket = htmlspecialchars($data["ket"]);
    $gambarLama = htmlspecialchars($data["gambarLama"]);
    

    //cek apakah user pilih gambar atau tidak
    if ( $_FILES['gambar']['error'] === 4 ) {
        $gambar = $gambarLama;
    } else {
        $gambar = upload();
    }

    //query insert data
    $query = "UPDATE jadwal SET
                judul = '$judul',
                gambar = '$gambar',
                hari = '$hari',
                waktu = '$waktu',
                ket = '$ket'
            WHERE id_jadwal = $id
                ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function cari($keyword) {
    $query = "SELECT * FROM jadwal
                WHERE
            id_jadwal LIKE '%$keyword%' OR
            judul LIKE '%$keyword%' OR
            hari LIKE '%$keyword%' OR
            waktu LIKE '%$keyword%' OR
            ket LIKE '%$keyword%'
    ";

    return query($query);
}

function registrasi($data) {
    global $conn;

    $username = strtolower(stripslashes($data["username"]));
    $email = strtolower(stripslashes($data["email"]));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);

    // cek username sudah ada atau belum
    $result = mysqli_query($conn, "SELECT username, email FROM user WHERE username = '$username' OR email = '$email'");

    // mengatasi user tidak mengisi dengan spasi
    if ( mysqli_fetch_assoc($result) ) {
        echo "<script>
        alert('username/email sudah terdaftar!')
        </script>";
        return false;
    }
    
    if ( empty(trim( $username & $email )) ) {
        echo "<script>
                alert('field username/email tidak boleh kosong!');
                </script>
        ";
        return false;
    }
    //cek konfirmasi password
    if ( $password !== $password2 ) {
        echo "<script>
                alert('konfirmasi password tidak sesuai!')
                </script>";
            return false;
    }

    // ekripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    // tambahkan userbaru ke database
    mysqli_query($conn, "INSERT INTO user VALUES('', '$username', '$email',  '$password')");

    return mysqli_affected_rows($conn);

}