<?php
session_start();
require_once 'functions.php';

// cek cookie
if ( isset($_COOKIE['id']) && isset($_COOKIE['key']) ) {
	$id = $_COOKIE['id'];
	$key = $_COOKIE['key'];

	// ambil username berdasarkan id
	$result = mysqli_query($conn, "SELECT username FROM user WHERE id= $id");
	$row = mysqli_fetch_assoc($result);

	// cek cookie dan username
	if( $key === hash('sha256', $row['username']) ) {
		$_SESSION['login'] = true;
	}
}

if( isset($_SESSION["login"]) ) {
	header("Location: index.php");
	exit;
}


if( isset($_POST["register"]) ) {

	if( registrasi($_POST) > 0 ) {
		echo "<script>
				alert('user baru berhasil ditambahkan');
				</script>";
	} else {
		echo mysqli_error($conn);
	}

}

if( isset($_POST["login"]) ) {
	
	$username = $_POST["username_login"];
	$password = $_POST["password_login"];

	$result = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");

	// cek username
	if( mysqli_num_rows($result) === 1 ) {

		//cek password
		$row = mysqli_fetch_assoc($result);
		if( password_verify($password, $row["password"]) ) {
			// set session
			$_SESSION["login"] = true;

			// cek remember me
			if ( isset($_POST['remember']) ) {
				// buat cookie
				setcookie('id', $row['id'], time()+60);
				setcookie('key', hash('sha256', $row['username']), time()+60);
			}

			header("Location: index.php");
			exit;
		}
	}

	$error = true;
}


?>




<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<title>user login</title>
</head>
<body>
	<div class="align">
		<img class="logo" src="assets/img/logo.svg">
		<div class="card">
			<div class="head">
				<div></div>
				<a id="login" class="selected" href="#login">Login</a>
				<a id="register" href="#register">Register</a>
				<div></div>
			</div>
			<div class="tabs">
				<form action="" method="post">
					<div class="inputs">
						<!-- <?php if( isset($error) ) : ?>
							<p style="color: red; font-style: italic;">username/password salah</p>
						<?php endif ; ?> -->
						<div class="input">
							<input placeholder="Username" type="text" name="username_login">
							<img src="assets/img/user.svg">
						</div>
						<div class="input">
							<input placeholder="Password" type="password" name="password_login">
							<img src="assets/img/pass.svg">
						</div>
						<label class="checkbox">
							<input type="checkbox" name="remember">
							<span>Remember me</span>
						</label>
					</div>
					<button type="submit" name="login">Login</button>
				</form>
				<form action="" method="post">
					<div class="inputs">
						<div class="input">
							<input placeholder="Email" type="text" name="email" required>
							<img src="assets/img/mail.svg">
						</div>
						<div class="input">
							<input placeholder="Username" type="text" name="username" required>
							<img src="assets/img/user.svg">
						</div>
						<div class="input">
							<input placeholder="Password" type="password" name="password" required>
							<img src="assets/img/pass.svg">
						</div>
						<div class="input">
							<input placeholder="Konfirmasi Password" type="password" name="password2" required>
							<img src="assets/img/pass.svg">
						</div>
					</div>
					<button type="submit" name="register">Register</button>
				</form>
			</div>
		</div>
	</div>
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/index.js"></script>
</body>
</html>
