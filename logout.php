<?php

session_start();
$_SESSION = [];
session_unset();
session_destroy();

set_cookie('id', '', time() - 3600);
set_cookie('key', '', time() - 3600);

header("Location: logindanregis.php");
exit;


?>