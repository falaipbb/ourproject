<?php
session_start();

if( !isset($_SESSION["login"]) ) {
    header("Location: logindanregis.php");
    exit;
}

require_once 'functions.php';

$id = $_GET["id_jadwal"];

if( hapus($id) > 0) {
    echo "
        <script>
        alert('data berhasil dihapus!');
        document.location.href = 'index.php';
        </script>
    ";
} else {
    echo "
        <script>
        alert('data berhasil ditambahkan!');
        document.location.href = 'index.php';
        </script>
    ";
}
?>