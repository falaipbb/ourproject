<?php
session_start();

if( !isset($_SESSION["login"]) ) {
    header("Location: logindanregis.php");
    exit;
}
// Create database connection using function file
require_once('functions.php');

// pagination
//konfigurasi
$jumlahDataPerHalaman = 6 ;
$jumlahData = count(query("SELECT * FROM jadwal ORDER BY id_jadwal ASC"));
$jumlahHalaman = ceil($jumlahData / $jumlahDataPerHalaman);
$halamanAktif = ( isset($_GET["page"]) ) ? $_GET["page"] : 1;
$awalData = ( $jumlahDataPerHalaman * $halamanAktif  ) - $jumlahDataPerHalaman;


// Fetch all jadwal data from database
// ket last(mulai dari(index), tampilkan berapa data yg mau ditampilkan)
$jadwal = query("SELECT * FROM jadwal LIMIT $awalData, $jumlahDataPerHalaman");

//tombol cari di tekan
    if( isset($_POST["cari"]) ) {
        $jadwal = cari($_POST["keyword"]);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Keseharian</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="assets/js/jquery-3.5.0.min.js"></script>
    <script src="assets/js/script.js"></script>

    <link rel="stylesheet" href="style.css">
</head>
<style>
        input, select, textarea{
            padding: 10px;
            width: 100%;
            height: 100%;
        }
        .loader{
            width: 8%;
            position: absolute;
            right: -15px;
            top: 100px;
            display: none;
        }
    </style>
<body>
<div class="wrapper" id="wrapper">
    <div class="sidebar">
    <h2>Fauzi</h2>
    <ul>
    <li><a href="index.php"><i class="fas fa-home"></i>Home</a></li>
    <li><a href="input.php"><i class="fas fa-plus"></i>Tambah data</a></li>
    </ul>
    <div class="social_media">
        <a href="https://www.facebook.com/profile.php?id=100009076621845" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="https://twitter.com/Fauzi33372888" target="_blank"><i class="fab fa-twitter"></i></a>
        <a href="https://www.instagram.com/jipay_fp/" target="_blank"><i class="fab fa-instagram"></i></a>
    </div>
    </div>
    <div class="main_content">
        <div class="header">Welcome!! 
            <a href="logout.php">logout</a>
            <h2>Daftar keseharian saat ini</h2>
        </div>
            <br>
            <form action="" method="post">
                    <input type="text" name="keyword" size="75" autofocus placeholder="masukkan keyword pencarian.." autocomplete="off" id="keyword">     
                    <button type="submit" name="cari" id="tombol-cari">Cari!</button>

                    <img src="assets/img_tab/load.gif" alt="" class="loader">
            </form>
            <div class="print">
                <a href="print.php" target="_blank"><i class="fas fa-print"></i></a>
            </div>
        <div class="container" id="container">
            <table id='t01'>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Gambar</th>
                <th>Waktu</th>
                <th>Hari</th>
                <th>Manfaat</th>
                <th colspan='2'>Action</th>
            </tr>
            <?php 
                $no=1;
                foreach( $jadwal as $j ) { ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $j['judul']; ?></td>
                <td><img src="assets/img_tab/<?= $j['gambar']; ?>" width="80" height="50" alt="gambar tidak ada"></td>
                <td><?= substr($j['waktu'],0,-3); ?></td>
                <td><?= $j['hari']; ?></td>
                <td><?= $j['ket']; ?></td>
                <td>
                <a href='edit.php?id_jadwal=<?= $j['id_jadwal']; ?>'>Edit</a> 
                
                <a href="delete.php?id_jadwal=<?= $j['id_jadwal']; ?>" onclick="return confirm('Yakin ingin dihapus?');" >Delete</a>
                </td>
            </tr>
            <?php } ?>
            </table>
        </div>
    <!-- navigasi -->
        <div class="pagination">

        <?php if( $halamanAktif > 1) : ?>
            <a href="?page=<?= $halamanAktif - 1; ?>">&laquo;</a>
        <?php endif; ?>

    <?php for( $i = 1; $i <= $jumlahHalaman; $i++) : ?>
        <?php if( $i == $halamanAktif ) : ?>
            <a href="?page=<?= $i; ?>" style="font-weight: bold; color: red;"><?= $i; ?></a>
        <?php else : ?> 
            <a href="?page=<?= $i; ?>"><?= $i; ?></a>
        <?php endif; ?>
    <?php endfor ;  ?>

        <?php if( $halamanAktif < $jumlahHalaman ) : ?>
            <a href="?page=<?= $halamanAktif + 1; ?>">&raquo;</a>
        <?php endif; ?>

        </div>
    </div>
</div>

</body>
</html>