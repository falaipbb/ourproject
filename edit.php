<?php
session_start();

if( !isset($_SESSION["login"]) ) {
    header("Location: logindanregis.php");
    exit;
}

require_once 'functions.php';

// ambil data di url
$id = $_GET["id_jadwal"];

// query data jadwal berdasarkan id
$jadwal = query("SELECT * FROM jadwal WHERE id_jadwal = $id")[0];

// cek apakah tombil submit sudah ditekan atau belum
if ( isset($_POST["submit"]) ) {

    // cek apakah data berhasil diubah atau tidak
    if( ubah($_POST) > 0 ) {
        echo "
            <script>
                alert('data berhasil diubah!');
                document.location.href = 'index.php';
            </script>
        
        ";
    } else {
        echo "
            <script>
                alert('data gagal diubah!');
                document.location.href = 'index.php';
            </script>
        ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah data</title>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <link rel="stylesheet" href="style.css">
    <style>
        th, td {
    padding: 0px;
    text-align: center;
        }
        input, select, textarea{
            padding: 15px;
            width: 100%;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="sidebar">
    <h2>Fauzi</h2>
    <ul>
    <li><a href="index.php"><i class="fas fa-home"></i>Home</a></li>
    <li><a href="input.php"><i class="fas fa-plus"></i>Tambah data</a></li>
    </ul>
    <div class="social_media">
        <a href="https://www.facebook.com/profile.php?id=100009076621845" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="https://twitter.com/Fauzi33372888" target="_blank"><i class="fab fa-twitter"></i></a>
        <a href="https://www.instagram.com/jipay_fp/" target="_blank"><i class="fab fa-instagram"></i></a>
    </div>
    </div>
    <div class="main_content">
        <div class="header">Welcome!! </div>
        <table id='t01'>
        <form action="" method="post" enctype="multipart/form-data">
    <caption><h2>Update Data</h2></caption>
            <input type="hidden" name="id" value="<?= $jadwal['id_jadwal'] ?>">
            <input type="hidden" name="gambarLama" value="<?= $jadwal['gambar']; ?>">
        <tr>
            <th>Judul</th>
            <td>:</td>
            <td><input type="text" name="judul" value="<?= $jadwal['judul'] ?>"></td>
        </tr>
        <tr>
            <th><label for="gambar">Gambar</label></th>
            <td>
            </td>
            <td>
            <img src="assets/img_tab/<?= $jadwal['gambar']; ?>" width="100" alt="">
            <input type="file" name="gambar" id="gambar">
            </td>
        </tr>
        <tr>
            <th>Hari</th>
            <td>:</td>
            <td>
            <?php $hari = $jadwal['hari']; ?>
            <select name="hari" id="">
                <option <?= ($hari == 'senin') ? "selected": "" ?>>senin</option>
                <option <?= ($hari == 'selasa') ? "selected": "" ?>>selasa</option>
                <option <?= ($hari == 'rabu') ? "selected": "" ?>>rabu</option>
                <option <?= ($hari == 'kamis') ? "selected": "" ?>>kamis</option>
                <option <?= ($hari == 'jumat') ? "selected": "" ?>>jumat</option>
                <option <?= ($hari == 'sabtu') ? "selected": "" ?>>sabtu</option>
                <option <?= ($hari == 'minggu') ? "selected": "" ?>>minggu</option>
            </select>
            </td>
        </tr>
        <tr>
            <th>Waktu</th>
            <td>:</td>
            <td><input type="time" name="waktu" value="<?= $jadwal['waktu'] ?>"></td>
        </tr>
        <tr>
            <th>Keterangan</th>
            <td>:</td>
            <td>
            <textarea name="ket" id="" cols="30" rows="10"><?=  $jadwal['ket'] ?></textarea>
            </td>
        </tr>
        <tr>
            <th>Action</th>
            <td>:</td>
            <td><input type="submit" style=background:tomato name="submit" value="submit"></td>
        </tr>
    </form>
    </table>
    </div>
</div>
    
</body>
</html>