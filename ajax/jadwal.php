<?php 
session_start();

if( !isset($_SESSION["login"]) ) {
    header("Location: logindanregis.php");
    exit;
}

// sleep(1);// minimal 1 sec
usleep(500000);
require_once '../functions.php';

$keyword = $_GET["keyword"];

$query = "SELECT * FROM jadwal
            WHERE
            id_jadwal LIKE '%$keyword%' OR
            judul LIKE '%$keyword%' OR
            hari LIKE '%$keyword%' OR
            waktu LIKE '%$keyword%' OR
            ket LIKE '%$keyword%'
        ";
$jadwal = query($query);

?>


        <table id='t01'>
        <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Gambar</th>
            <th>Waktu</th>
            <th>Hari</th>
            <th>Manfaat</th>
            <th colspan='2'>Action</th>
        </tr>
        <?php 
            $no=1;
            foreach( $jadwal as $j ) { ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $j['judul']; ?></td>
            <td><img src="assets/img_tab/<?= $j['gambar']; ?>" width="80" height="50" alt="gambar tidak ada"></td>
            <td><?= substr($j['waktu'],0,-3); ?></td>
            <td><?= $j['hari']; ?></td>
            <td><?= $j['ket']; ?></td>
            <td>
            <a href='edit.php?id_jadwal=<?= $j['id_jadwal']; ?>'>Edit</a> 
            
            <a href="delete.php?id_jadwal=<?= $j['id_jadwal']; ?>" onclick="return confirm('Yakin ingin dihapus?');" >Delete</a>
            </td>
        </tr>
        <?php } ?>
    </table>
    