-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Apr 2020 pada 06.31
-- Versi server: 10.1.28-MariaDB
-- Versi PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pay`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama_admin` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(25) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama_admin`, `password`, `email`, `jurusan`, `picture`) VALUES
(7, 'masmus', '22', 'masmus@gmail.com', 'komputer', '1.jpg'),
(12, 'alex', '$2y$10$tPtINtB9CpSfgp2ky9', 'alex@gmail.com', 'industri', '2.jpg'),
(13, 'as', '$2y$10$0VmJNkCRTkL3XIOR2B3EW.YfadcOUtQhvPw1x4ZDbn5x5hETB0fry', 'as@gmail.com', 'tataboga', '3.jpg'),
(14, 'fauzi', '$2y$10$n0XmRIr8uROwNpP5SVm8X.h6zHDql3STGKGyllRMg6V4TqAf1qe.y', 'fauzi@gmail.com', 'web', '4.jpg'),
(16, 'pay', '$2y$10$WN2Yk5BEYeWZ/q9bO0y7OOH.2PJ7uiFuNSM7r6jsdw.WxthdG3Vii', 'pay@gmail.com', 'pengacara', '5.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `gambar` varchar(1000) NOT NULL,
  `hari` varchar(20) DEFAULT NULL,
  `waktu` time DEFAULT NULL,
  `ket` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `judul`, `gambar`, `hari`, `waktu`, `ket`) VALUES
(1, 'Shalat Subuh', '5e9a71230bb55.png', 'senin', '04:35:00', 'Dari sisi kesehatan, bangun pagi untuk melaksanakan shalat Subuh pun mampu menormalkan kinerja syaraf dan otak. Apalagi saat pagi hari kadar ozon (O3) cukup tinggi yang mampu membantu aktivitas syaraf dan otak.         '),
(2, 'Baca Al Quran', '5e9a71413fe35.jpg', 'senin', '06:00:00', '            Orang yang rajin membaca Alquran terbukti memiliki daya ingat yang lebih baik. Selain itu, membaca Alquran, juga secara otomatis mampu membuat otak lebih fokus, sehingga lebih mudah untuk berkonsentrasi\r\n            '),
(3, 'Olahraga', '5e9a714b56fa6.png', 'senin', '07:00:00', 'Meningkatkan energi tubuh, Melakukan olahraga di pagi hari akan membuat tubuh mendapatkan oksigen dalam jumlah yang cukup. Kondisi ini akan memudahkan nutrisi masuk ke dalam jantung dan juga paru secara maksimal                    '),
(11, 'Shalat Dhuha', '5e9a71564c639.png', 'senin', '09:00:00', 'Shalat dhuha dilaksanakan sebagai salah satu bentuk sedekah bagi organ tubuh sebagaimana disebutkan dalam hadist berikut, â€œSetiap pagi, setiap ruas anggota badan kalian wajib dikeluarkan sedekahnya. Setiap tasbih adalah sedekah, setiap tahmid adalah sedekah, setiap tahlil adalah sedekah, setiap takbir adalah sedekah, menyuruh kepada kebaikan adalah sedekah, dan melarang berbuat munkar adalah sedekah. Semua itu dapat diganti dengan shalat dhuha dua rakaat.â€ (HR. Muslim)\r\n            '),
(12, 'Belajar ', '5e9a715f871ca.png', 'senin', '10:00:00', 'Dengan belajar dapat menumbuhkan kebiasaan pada diri orang tersebut. Dengan belajar dapat menumbuhkan motifasi pada diri orang tersebut dan dapat menjadikan seseorang sukses. Dengan belajar akan menambah banyak ilmu pengetahuan.'),
(13, 'Istirahat', '5e9a80f088a80.jpg', 'senin', '01:15:00', 'Manfaat tidur siang bagi orang dewasa\r\nMeningkatkan daya ingat\r\nTidur siang dapat membantu meningkatkan daya ingat, terutama mengenai hal-hal yang telah dipelajari pada awal hari.\r\nLebih mudah mengumpulkan informasi\r\nTidur siang dapat membantu otak menghubungkan berbagai hal yang Anda ketahui. Selain itu, tidur siang juga bisa membuat Anda merasa lebih mudah mengumpulkan informasi yang diperoleh pada hari sebelumnya.\r\nRutinlah tidur siang. Beberapa penelitian menunjukkan bahwa orang yang rutin tidur siang biasanya memiliki kualitas tidur yang lebih baik di malam harinya. Oleh sebab itu, tak ada salahnya bagi Anda untuk mulai rutin melakukan tidur siang.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(12, 'pay', 'falaipbb@gmail.com', '$2y$10$AqZIZB2ogQO/Q6A6MfuXmegGk6KU.m.kwpf13XVKJefJHkTThIPtG');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
